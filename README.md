	This program implements the algorithm of local search for the vehicle routing problem. 
The vehicle routing problem (VRP) is a combinatorial optimization and integer programming 
problem which asks "What is the optimal set of routes for a fleet of vehicles to traverse 
in order to deliver to a given set of customers?". It generalises the well-known travelling 
salesman problem (TSP). The objective of the VRP is to minimize the total route cost. Determining 
the optimal solution is an NP-hard problem in combinatorial optimization, so heuristics are usally used. 
	The program takes as input a file having the form:
Id, X, Y
where Id is a unique number for every customer and X, Y are his coordinates. The file example can be used as input file.
	We use the random-restart hill climbing technique, which belongs to the family of local search. We implement the following algorithm:
1. S* < an initial solution ;
2. loop do

	(a) 
	    
	    S < a new random initial solution ;
	
		end < false ;
		
	(b) While not end :
	
		S' < choose a neighbour(S) ;
		
		if S' > S then S < S' ; 
		
		else end < true ;
		
	(c) if S > S* then S* < S ;
		
3. return(S*).

	A solution is a list of paths, each one of them is a list of clients. A solution S' is a neighbour of S,
if all of its clients have the same position in every path as in S, except one client, which is moved to 
another path, or to another position in the same path. A solution S' is better than a solution S if the 
total distance of S' is smaller than the total distance of S.
	The class problem reads a file and then creates an arraylist of the clients from the file. The class client 
describes a client, having his attributes (Id and coordinates). This class also has a function dist, which 
calculates the distance between two clients. The class trajet describes a path, so it contains an arraylist 
of clients and a function dist, that calculates the total distance of the path. It also contains functions 
that allow to modify the path. The class solution represents a solution for the problem, so it contains an 
arraylist with the paths of the trucks and a function that designs this solution. The class dessin is used 
for desplaying a solution.
	The function main is in the class routage_aleatoire. In the main function we implement the random-restart hill 
climbing algorithm as described above. We use the function choisir_voisin in order to find the best neighbour of a 
solution. This function uses the function modifier which returns a neighbour of a solution.
	There are two frames: one depicts the best solution that has been found and the other depicts the best 
neighbours until we find a local optimum every time.