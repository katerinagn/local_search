package projet1;

import java.util.* ; import java.util.List;
import java.io.* ; import java.awt.* ; 

import javax.swing.* ;


public class routage_aleatoire {


	static solution modifier(solution sol, int i,int j,int k,int l,int N){
		
		solution s=new solution(sol,N);
		client c=s.trajets.get(i).clients.get(j);

		trajet t=s.trajets.get(k);

		trajet t1=s.trajets.get(i);
		
		t.add(l, c);
		if ((i==k) && (j>l))
			t1.remove(j+1,c);
		else 
			t1.remove(j,c);

		
		double dis=0;
		for(int f=0;f<N;f++) 
			dis += s.trajets.get(f).dist() ;
		
		s.setdis(dis,s);

		return s;

		
	}
	
	
	static solution choisir_voisin(solution sol, int N){
		
		
		solution newS,s;
		solution Scour=new solution(sol,N);
		
		int [] len=new int [N];
		int i,j,k,l;
		
		double dis=0;
		
		for (i=0; i<N;i++){
			len[i]=sol.trajets.get(i).clients.size();

		}
		
		for(i=0; i<N; i++)
			if (len[i]>1)
			for(j=0; j<len[i]; j++)
				for (k=0; k<N; k++)
					for (l=0; l<=len[k]; l++)
						if ((i!=k) || (j!=l)){
							s=new solution(sol,N);
							newS=modifier(s,i,j,k,l,N);
							
							if (newS.dist<Scour.dist)
								Scour=new solution(newS,N);
						}	
					
		return Scour;
	}
	

	
	public static void main (String [] arg) {
		JFrame carte = new JFrame("Best solution found") ;
		carte.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ; 
		carte.setSize(550,550) ;
		
		JFrame carte1 = new JFrame("Best neighbours until local optimum ") ;
		carte1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ; 
		carte1.setSize(550,550) ;
		
		final JFileChooser fc = new JFileChooser(); 
		fc.setCurrentDirectory(new File(".")) ;
		
		int returnVal = fc.showOpenDialog(carte) ; 
		String fileName = fc.getSelectedFile().getPath() ;
		probleme P = new probleme(fileName) ;
	
		solution S = new solution(3,P) ;

		
		solution ns=choisir_voisin(S,3);
		
	
 		double prev=S.dist;
		boolean fini=false;
		while (true) {	
			for (int n=2; n<5; n++){
			
				S = new solution(n,P) ;
				fini=false;
				
				while (!fini){ 
					 ns=choisir_voisin(S,n);
				
					 if (ns.dist<S.dist){
						 S= new solution(ns,n);
						 carte1.add(new dessin (fileName, P,S,5)) ; 
						 carte1.setVisible(true) ;
					 }
					 else fini=true;
				}  
								 
		
				if (S.dist<prev){
					System.out.println(S.dist);
			
					carte.add(new dessin (fileName, P,S,5)) ; 
					carte.setVisible(true) ; 
					prev=S.dist; 
				}
					
			}
					
		}
	}
}
