package projet1;

import java.util.* ; import java.util.List;
import java.io.* ; import java.awt.* ; 
import javax.swing.* ;


public class solution extends JPanel {
	double dist ; 
	int nbCamions ; 
	ArrayList<trajet> trajets = new ArrayList<trajet>();
	
	public void setdis(double d,solution s){
		s.dist=d;
	}
	
	solution(solution s,int N){
		this.dist = s.dist ;
		this.nbCamions = N ; 
	
		 trajets = new ArrayList<trajet>();
		for (trajet t:s.trajets)
			this.trajets.add(new trajet(t));
	
		
	}
		
	solution(int N, probleme P) { 
		int i ; 
		Random R = new Random() ;
		client depot = P.depot ;  
		nbCamions = N ; 
		dist = 0 ;
		for(i=0;i<N;i++) 
			trajets.add(new trajet(depot)) ;
		for(client C : P.clients) { 
			trajet T = trajets.get(R.nextInt(N)) ;
			if (T.clients.size()==0) 
				T.add(0,C) ; 
			else 
				T.add(R.nextInt(T.clients.size()),C) ; 
	} 
	// Calcul de la distance totale parcourue par tous les camions - 
	//Calculation of the total distance travelled by all trucks

	for(i=0;i<N;i++) 
		dist += trajets.get(i).dist ; 
	}
// DESSIN



	void dessiner(Graphics g, int ech) {
		ArrayList<Color> couls =  new ArrayList<Color>
			(Arrays.asList(Color.GREEN,Color.RED,Color.BLUE,Color.BLACK,Color.ORANGE,Color.MAGENTA)) ;
		int i = 0 ; 
	
		for(trajet T : trajets) { 
		
			T.dessiner(g,couls.get(i%6),ech) ; i++ ; 
		
		}
	}


}
