package projet1;

import java.util.* ; import java.io.* ; import java.awt.* ; 
import javax.swing.* ;


public class trajet {
	double dist ; 
	client depot ; 
	ArrayList<client> clients = new ArrayList<client>() ;
	
	trajet(trajet t){
		this.dist=t.dist;
		this.depot=t.depot;
		clients= new ArrayList<client>();

		for(int i=0; i<t.clients.size(); i++) { 
			
			this.clients.add(t.clients.get(i)) ;
		
		}
	}
	
		
	trajet (client d) { 
		depot = d ; 
		dist = 0 ; 
	}
	
	// Calcul de la distance totale parcourue sur un trajet -
	//Calculation of the total distance of one path :
	double dist() { 
		client c = depot ; 
		double d = 0 ;
		if (clients.size() == 0) 
			return(0) ;
		for(client C : clients) { 
			d += c.dist(C) ; 
			c = C ; 
		} 
		return(d + c.dist(depot)) ; 
	}
	
	void add(int pos, client c) { 
		client Cprec, Csuiv;
		if (pos==clients.size() && (pos!=0)){
			Csuiv = depot ;
			Cprec = clients.get(pos-1) ;
			clients.add(c) ;
			dist += Cprec.dist(c) + Csuiv.dist(c) - Cprec.dist(Csuiv) ; 
		}
		else{	
			if(clients.size()>0) 
				Csuiv = clients.get(pos) ; 
			else Csuiv = depot ;
			if (pos!=0) 
				Cprec = clients.get(pos-1) ; 
			else Cprec = depot ;   
			
			clients.add(pos, c) ;
			dist += Cprec.dist(c) + Csuiv.dist(c) - Cprec.dist(Csuiv) ; 
		} 
	}
	
	void remove(int pos, client c) { 
		client Cprec, Csuiv;
		if((clients.size()>1) && (pos!=clients.size()-1))
			Csuiv = clients.get(pos) ; 
		else Csuiv = depot ;
		if (pos!=0) 
			Cprec = clients.get(pos-1) ; 
		else Cprec = depot ;   
		
		dist -= Cprec.dist(c) + Csuiv.dist(c) - Cprec.dist(Csuiv) ; 
		clients.remove(pos) ;
	}
	
	
	// dessin du trajet sur une carte - 
	//draw a path
	public void dessiner(Graphics g, Color col, int ech) { 
		g.setColor(col) ;
		if (clients.size() != 0) { 
			client c = depot ;
			for(client C : clients) { 
				g.drawLine(ech*c.x,ech*c.y,ech*C.x,ech*C.y) ; 
				c = C ; 
			}
			g.drawLine(ech*c.x,ech*c.y,ech*depot.x,ech*depot.y) ; 
		} 
	}


}
