package projet1;

import java.util.* ; import java.io.* ; import java.awt.* ; import javax.swing.* ;


public class dessin extends JPanel {
	String fileName ; probleme P ; solution S ; int echelle ;
	dessin(String f , probleme P, solution S, int E) { 
		this.fileName = f ; 
		this.P = P ; 
		this.S = S ; 
		echelle = E ; 
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g); this.setBackground(Color.WHITE) ;
		P.dessiner(g,echelle) ; 
		S.dessiner(g,echelle) ;
		g.setColor(Color.BLACK) ;
		g.drawString("File chosen : "+fileName,10*echelle,90*echelle) ;
		g.drawString("Random solution with "+String.valueOf(S.nbCamions)+" trucks",10*echelle,95*echelle) ;
		g.drawString("Total distance travelled : "+String.valueOf(S.dist),10*echelle,100*echelle) ; 
	}

}
