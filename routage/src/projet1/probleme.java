package projet1;

import java.util.* ; import java.io.* ; import java.awt.* ; import javax.swing.* ;


public class probleme extends JPanel {
 
	client depot ; 
	ArrayList<client> clients = new ArrayList<client>() ;

 	probleme (String nomFichier) { 
 		String ligne, token[] ; 
 		int t[] = new int[7], i;
		try {
			BufferedReader fichier 	= new BufferedReader(new FileReader(nomFichier));
			System.out.println("open file ok") ;
			token = fichier.readLine().split("\\s+") ;
			for(i=0;i<3;i++) 
				t[i] = Integer.parseInt(token[i]) ;
			depot = new client(t[0],t[1],t[2]) ;
			while((ligne = fichier.readLine()) != null) { 
				token = ligne.split("\\s+") ;
				for(i=0;i<3;i++) 
					t[i] = Integer.parseInt(token[i]) ;
			  clients.add(new client(t[0],t[1],t[2])) ; }
		} 
		catch(IOException ioe) { 
			System.out.println("pb file !!") ; 
		} 
	}

	void dessiner(Graphics g, int ech) {
		g.fillRect(ech*depot.x-5,ech*depot.y-5,10,10) ;
		for(client C : clients) g.fillOval(ech*C.x-2,ech*C.y-2,4,4) ; }
}
